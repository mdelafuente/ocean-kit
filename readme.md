# Ocean Kit
- Current goal: Move <!-- tf -->away from Figma
- Current problem: There's no suitable replacement ready yet, only options that are closer to it than others. Penpot is FOSS, but has big issues on performance due to relying on the browser to render shadows, missing some critical features and some quirks on rendering text etc.
- Solution: Short term we'll try using Lunacy for hi-res mockups instead, which is a free-as-in-beer design app developed by Icons8. Long term, a FOSS app like Penpot or Akira would be ideal.

---

## How to set up Lunacy
1. Install Lunacy via one of the following
    - [Flathub](https://flathub.org/apps/details/com.icons8.Lunacy)
    - [Snap](https://snapcraft.io/lunacy)
    - [.deb Binary, x86_64](https://lcdn.icons8.com/setup/Lunacy.deb)
    - [.deb Binary, ARM](https://lcdn.icons8.com/setup/Lunacy.ARM.deb)

    For other options like macOS or Windows, see their website at https://icons8.com/lunacy

2. Install all fonts
    - Fedora: `sudo dnf install google-noto-sans-display-fonts google-noto-sans-display-vf-fonts google-noto-sans-fonts google-noto-sans-vf-fonts`
        * Fedora [unfortunately doesn't have Hack packaged right now](https://bugzilla.redhat.com/show_bug.cgi?id=1258542), so you have to install the latest TTFs manually from [their GitHub repository](https://github.com/source-foundry/Hack/releases). Make sure to install it to the system instead of the user to avoid any potential issues.
    - openSUSE: `sudo zypper in noto-fonts hack-fonts`
    - Arch Linux: `sudo pacman -S noto-fonts ttf-hack`
    - Ubuntu: `sudo apt install fonts-noto fonts-hack-ttf`

3. Get the kit by doing one of the two options
    - [By downloading the repo as a zip](https://invent.kde.org/mdelafuente/ocean-kit/-/archive/master/ocean-kit-master.zip)
    - By cloning the repo with `git clone https://invent.kde.org/mdelafuente/ocean-kit.git`

## Some details
- Font rendering: Some minor keming in PNG and JPG @1x exports due to what seems to be very agressive hinting. SVG and @2x exports look perfect though. Just export to @2x regardless, many people are using HiDPI devices nowadays.
- No auto layout, [although it's planned to be released this year.](https://lunatics.icons8.com/discussion/194/auto-layout-a-la-figma)
